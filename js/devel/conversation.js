/**
 * Nextcloud - Phone Sync
 *
 * This file is licensed under the Affero General Public License version 3 or
 * later. See the COPYING file.
 *
 * @author Loic Blot <loic.blot@unix-experience.fr>
 * @copyright Loic Blot 2014-2018
 */

var Conversation = new Vue({
	el: '#app-content',
	data: {
		selectedContact: undefined,
		isConvLoading: false,
		messages: [],
		lastConvMessageDate: 0,
		totalMessageCount: 0,
		refreshIntervalId: null
	},
	methods: {
		fetch: function (contact) {
			// If contact is not null, we will fetch a conversation for a new contact
			if (contact != null) {
				this.selectedContact = contact;
				this.isConvLoading = true;
			}

			this.messages = [];
			this.lastConvMessageDate = 0;

			let self = this;
			$.getJSON(Sms.generateURL('/front-api/v1/conversation'), { 'phoneNumber': self.selectedContact.nav },
				function (jsondata, status) {
					let phoneNumberLabel = self.selectedContact.nav;

					if (typeof jsondata['phoneNumbers'] !== 'undefined') {
						const phoneNumberList = arrayUnique(jsondata['phoneNumbers']);
						phoneNumberLabel = phoneNumberList.toString();
					}

					// Reinit messages before showing conversation
					self.formatConversation(jsondata);

					if (typeof jsondata['contactName'] === 'undefined' || jsondata['contactName'] === '') {
						self.selectedContact.label = phoneNumberLabel;
						self.selectedContact.opt_numbers = "";
					}
					else {
						self.selectedContact.label = jsondata['contactName'];
						self.selectedContact.opt_numbers = phoneNumberLabel;
					}

					self.totalMessageCount = jsondata['msgCount'] !== undefined ? jsondata['msgCount'] : 0;
					self.isConvLoading = false;

					$('#app-content').scrollTop(1E10);

					// If refreshInterval is already bound, clear previous
					if (self.refreshIntervalId !== null) {
						clearInterval(self.refreshIntervalId);
					}

					self.refreshIntervalId = setInterval(self.refresh, 10000);
					$.getJSON(Sms.generateURL('/front-api/v4/messages/selected_sim_card'), { 'phoneNumber': self.selectedContact.nav },
						function (jsondata, status) {
							SimCardList.selectSimCard(jsondata)
						});
				}
			);
		},
		refresh: function () {
			var self = this;
			$.getJSON(Sms.generateURL('/front-api/v1/conversation'),
				{
					'phoneNumber': Conversation.selectedContact.nav,
					"lastDate": Conversation.lastConvMessageDate
				},
				function (jsondata, status) {
					var fmt = self.formatConversation(jsondata);
					var conversationBuf = fmt[1];
					if (conversationBuf === true) {
						$('#app-content').scrollTop(1E10);
						// This will blink the tab because there is new messages
						if (document.hasFocus() === false) {
							Sms.unreadCountCurrentConv += parseInt(fmt[0]);
							document.title = Sms.originalTitle + " (" + Sms.unreadCountCurrentConv + ")";
							SmsNotifications.notify(Sms.unreadCountCurrentConv + " unread message(s) in conversation with "
								+ Conversation.selectedContact.label);
						}

					}

					self.totalMessageCount = jsondata['msgCount'] !== undefined ? parseInt(jsondata['msgCount']) : 0;

				}
			);
		},
		getContactColor: function (contact_id) {
			return ContactRenderer.generateColor(contact_id);
		},
		// Return (int) msgCount, (str) htmlConversation
		formatConversation: function (jsondata) {
			// Improve jQuery performance
			let buf = false;
			// Improve JS performance
			let msgClass = '';
			let msgCount = 0;
			let self = this;
			let twemojiOptions = { base: OC.generateUrl('/apps/esms/js/twemoji/') };

			$.each(jsondata["conversation"], function (id, vals) {
				if (vals["type"] == 1) {
					msgClass = "recv";
				}
				else if (vals["type"] == 2) {
					msgClass = "sent";
				}
				else {
					msgClass = 'unknown';
				}

				// Store the greater msg date for refresher
				// Note: we divide by 100 because number compare too large integers
				if ((id / 100) > (self.lastConvMessageDate / 100)) {
					self.lastConvMessageDate = id;

					// Multiplicate ID to permit date to use it properly
					self.addConversationMessage({
						'id': id,
						'nc_id': vals['nc_id'],
						'db_id': vals['id'],
						'carrier_name': vals['carrier_name'],
						'card_number': vals['card_number'],
						'device_name': vals['device_name'],
						'card_slot': vals['card_slot'],
						'icc_id': vals['icc_id'],
						'sent': vals['sent'],
						'type': msgClass,
						'date': new Date(id * 1),
						'content': twemoji.parse(anchorme(escapeHTML(vals['msg'])), twemojiOptions)
					});
					if (vals['nc_id'] != -1) {
						var i = 0;
						for (var msg of self.messages) {
							if (msg['db_id'] == vals['nc_id']) {
								self.messages.splice(i, 1);
								break;
							}
							i++;
						}
					}
					buf = true;
					msgCount++;
				}

			});
			return [msgCount, buf];
		},
		/*
		* Conversation messagelist management
		*/
		addConversationMessage: function (msg) {
			this.messages.push(msg);
		},
		onSelectedSimCardChange: function () {
			$.ajax({
				type: 'POST',
				url: Sms.generateURL('/front-api/v4/messages/selected_sim_card'),
				data: {
					phoneNumber: Conversation.selectedContact.nav,
					auto: SimCardList.selectedSim.auto ? 1 : 0,
					cardNumber: SimCardList.selectedSim['card_number'],
					cardSlot: SimCardList.selectedSim['card_slot'],
					iccId: "",
					carrierName: SimCardList.selectedSim['carrier_name'],
					deviceName: SimCardList.selectedSim['device_name']
				},
				success: function (data) {

				}
			}
			);
		},
		sendMessage: function () {
			var self = this;
			var sms = {};
			sms['smsCount'] = 1;
			sms['smsDatas'] = [];
			sms['smsDatas'][0] = {};
			sms['smsDatas'][0]['date'] = Date.now();
			sms['smsDatas'][0]['nc_id'] = -1;
			sms['smsDatas'][0]['type'] = 2;
			sms['smsDatas'][0]['_id'] = -1;
			sms['smsDatas'][0]['mbox'] = 1;
			sms['smsDatas'][0]['sent'] = 0;
			sms['smsDatas'][0]['read'] = "true";
			sms['smsDatas'][0]['seen'] = "true";
			sms['smsDatas'][0]['address'] = Conversation.selectedContact.nav;
			if (!SimCardList.selectedSim.auto) {

				sms['smsDatas'][0]['card_number'] = SimCardList.selectedSim['card_number'];
				sms['smsDatas'][0]['card_slot'] = SimCardList.selectedSim['card_slot'];
				sms['smsDatas'][0]['icc_id'] = "";
				sms['smsDatas'][0]['carrier_name'] = SimCardList.selectedSim['carrier_name'];
				sms['smsDatas'][0]['device_name'] = SimCardList.selectedSim['device_name'];
			}
			else {
				//take last message
				var last = self.messages[self.messages.length - 1]
				if (last == undefined && SimCardList.list.length > 1)
					last = SimCardList.list[1]
				if (last != undefined) {
					sms['smsDatas'][0]['card_number'] = last['card_number'];
					sms['smsDatas'][0]['card_slot'] = last['card_slot'];
					sms['smsDatas'][0]['icc_id'] = last['icc_id'];
					sms['smsDatas'][0]['carrier_name'] = last['carrier_name'];
					sms['smsDatas'][0]['device_name'] = last['device_name'];
				} else {
					sms['smsDatas'][0]['card_number'] = "";
					sms['smsDatas'][0]['card_slot'] = -1;
					sms['smsDatas'][0]['icc_id'] = "";
					sms['smsDatas'][0]['carrier_name'] = "";
					sms['smsDatas'][0]['device_name'] = "";
				}
			}
			sms['smsDatas'][0]['body'] = $("#esms-message").val();
			$("#esms-message").val("");

			$.ajax({
				type: 'POST',
				url: Sms.generateURL('/push?format=json'),
				data: JSON.stringify(sms),
				dataType: "json",
				contentType: "application/json",
				success: function (data) {
					$("#esms-message").val("");
					self.refresh()
					ContactList.fetch(false)
				}
			}
			);
		},
		removeConversationMessage: function (msgId) {
			const len = this.messages.length;
			let self = this;
			for (var i = 0; i < len; i++) {
				var curMsg = this.messages[i];
				if (curMsg['id'] === msgId) {
					$.post(Sms.generateURL('/delete/message'),
						{
							"messageId": msgId,
							"phoneNumber": this.selectedContact.label
						}, function (data) {
							self.messages.splice(i, 1);
							if (self.messages.length === 0) {
								self.clear();
							}
						});
					return;
				}
			}
		},
		removeConversation: function () {
			var self = this;
			$.post(Sms.generateURL('/delete/conversation'), { "contact": self.selectedContact.label }, function (data) {
				self.clear();
			});
		},
		clear: function () {
			// Reinit main window
			this.selectedContact.label = "";
			this.selectedContact.opt_numbers = "";
			this.selectedContact.avatar = undefined;
			ContactList.removeContact(this.selectedContact);
			this.messages = [];
			this.selectedContact = {};
			OC.Util.History.pushState('');
			clearInterval(this.refreshIntervalId);
			this.refreshIntervalId = null;
		}
	},
	updated: function () {
		$('#esms-messages-container').scrollTop(1E10);
	},
	computed: {
		orderedMessages: function () {
			return _.orderBy(this.messages, ['date'], ['asc'])
		}
	}
});