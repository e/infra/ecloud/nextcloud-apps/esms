/**
 * Nextcloud - Phone Sync
 *
 * This file is licensed under the Affero General Public License version 3 or
 * later. See the COPYING file.
 *
 * @author Loic Blot <loic.blot@unix-experience.fr>
 * @copyright Loic Blot 2014-2018
 */

var SimCardList = new Vue({
	el: '#esms-sim-card-select',
	data: {
		list: [],
		selectedSim: "auto",
	},
	created: function () {
		this.reset();
		this.loadSimCardList();
	},
	methods: {
		reset: function () {
			this.selectedSim = "auto";
			this.list = [];
		},
		onSimChange: function (event) {
			for (var sim of this.list) {
				if (sim.text == event.target.value) {
					this.selectedSim = sim;
					Conversation.onSelectedSimCardChange()
				}

			}
		},
		selectSimCard: function (card) {
			if (card != undefined)
				for (var sim of this.list) {

					if (sim['card_number'] == card['card_number'] &&
						sim['card_slot'] == card['card_slot'] &&
						sim['device_name'] == card['device_name'] &&
						sim['carrier_name'] == card['carrier_name']) {
						sim.checked = true;
						this.selectedSim = sim
					}
					else sim.checked = false;
				}
			this.list.__ob__.dep.notify()

		},
		loadSimCardList: function () {
			let self = this;
			$.getJSON(Sms.generateURL('/front-api/v1/simCardList'), function (jsondata, status) {
				for (var sim of jsondata) {
					var text = "";
					if (sim['card_number'] != "")
						text += sim['card_number'] + " - "
					if (sim['card_slot'] != "")
						text += "slot " + sim['card_slot'] + " - "
					if (sim['device_name'] != "")
						text += sim['device_name'] + " - "
					if (sim['carrier_name'] != "")
						text += sim['carrier_name'] + " - "
					text = text.substr(0, text.length - 3)
					sim.auto = false;
					sim.text = text
				}
				var sim = {}
				sim.text = "Auto"
				sim.auto = true;
				sim.checked = true;
				self.list = [sim]
				Array.prototype.push.apply(self.list, jsondata);
				self.selectSimCard(this.selectedSim)

			});
		},
	},
	computed: {

	}
});