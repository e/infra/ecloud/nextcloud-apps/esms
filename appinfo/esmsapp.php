<?php
/**
 * Nextcloud - Phone Sync
 *
 * This file is licensed under the Affero General Public License version 3 or
 * later. See the COPYING file.
 *
 * @author Loic Blot <loic.blot@unix-experience.fr>
 * @copyright Loic Blot 2014-2016
 */


namespace OCA\ESms\AppInfo;


use \OCP\AppFramework\App;
use \OCP\IContainer;

use \OCA\ESms\Controller\ApiController;
use \OCA\ESms\Controller\SettingsController;
use \OCA\ESms\Controller\SmsController;

use \OCA\ESms\Db\Sms;
use \OCA\ESms\Db\SmsMapper;
use \OCA\ESms\Db\ConversationStateMapper;

use \OCA\ESms\Db\ConfigMapper;

use \OCA\ESms\Migration\FixConversationReadStates;

use \OCA\DAV\CardDAV\CardDavBackend;

class ESmsApp extends App {

	/**
	 * ESmsApp constructor.
	 * @param array $urlParams
	 */
	public function __construct (array $urlParams=array()) {
		parent::__construct('esms', $urlParams);
		$container = $this->getContainer();
		$server = $container->query('ServerContainer');

		$container->registerService('UserId', function($c) use ($server) {
			if ($server->getUserSession()->getUser()) {
				return $server->getUserSession()->getUser()->getUID();
			}
			else {
				return null;
			}
		});

		/**
	         * Database Layer
        	 */
		$container->registerService('ConfigMapper', function (IContainer $c) use ($server) {
			return new ConfigMapper(
				$server->getDatabaseConnection(),
				$c->query('UserId'),
				$server->getCrypto()
			);
		});

		$container->registerService('Sms', function(IContainer $c) use ($server) {
			return new Sms($server->getDb());
		});

		$container->registerService('ConversationStateMapper', function(IContainer $c) use ($server) {
			return new ConversationStateMapper($server->getDatabaseConnection());
		});

		$container->registerService('SmsMapper', function(IContainer $c) use ($server) {
			return new SmsMapper(
				$server->getDatabaseConnection(),
				$c->query('ConversationStateMapper')
			);
		});

		/**
		 * Managers
		 */
		$container->registerService('ContactsManager', function(IContainer $c) use ($server) {
			return $server->getContactsManager();
		});

		/**
		 * Controllers
		 */
		$container->registerService('SettingsController', function(IContainer $c) {
			return new SettingsController(
				$c->query('AppName'),
				$c->query('Request'),
				$c->query('ConfigMapper')
			);
		});

		$container->registerService('SmsController', function(IContainer $c) use($server) {
			return new SmsController(
				$c->query('AppName'),
				$c->query('Request'),
				$c->query('UserId'),
				$c->query('SmsMapper'),
				$c->query('ConversationStateMapper'),
				$c->query('ConfigMapper'),
				$server->getContactsManager(),
				$server->getURLGenerator(),
				$server->getDatabaseConnection()
			);
		});

		$container->registerService('ApiController', function(IContainer $c) {
			return new ApiController(
				$c->query('AppName'),
				$c->query('Request'),
				$c->query('UserId'),
				$c->query('SmsMapper')
			);
		});

		/**
		 * Migration services
		 */
		$container->registerService('OCA\ESms\Migration\FixConversationReadStates', function ($c) {
			return new FixConversationReadStates(
				$c->query('ConversationStateMapper'),
				$c->getServer()->getUserManager()
			);
		});
	}
}
