OC.L10N.register(
    "esms",
    {
    "Phone Sync" : "SMS sync",
    "Cancel" : "Prekliči",
    "Confirm" : "Potrdi",
    "Settings" : "Nastavitve",
    "Default country code" : "Privzeta koda države",
    "Last message" : "Zadnje sporočilo",
    "Label" : "Oznaka",
    "Notification settings" : "Nastavitve obveščanja",
    "Enable" : "Omogoči",
    "Disable" : "Onemogoči",
    "Reset all messages" : "Ponastavi vsa sporočila",
    "Please select a conversation from the list to load it." : "Izberite pogovor s seznama."
},
"nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);");
