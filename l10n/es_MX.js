OC.L10N.register(
    "esms",
    {
    "Phone Sync" : "SMS sync",
    "An app to sync SMS with your cloud" : "Una aplicación para sincronizar SMS con tu nube",
    "Cancel" : "Cancelar",
    "Confirm" : "Confirmar",
    "Settings" : "Configuraciones ",
    "Label" : "Etiqueta",
    "Enable" : "Habilitar",
    "Disable" : "Deshabilitar"
},
"nplurals=2; plural=(n != 1);");
