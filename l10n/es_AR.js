OC.L10N.register(
    "esms",
    {
    "Phone Sync" : "SMS sync",
    "Cancel" : "Cancelar",
    "Confirm" : "Confirmar",
    "Settings" : "Ajustes",
    "Label" : "Etiqueta",
    "Notification settings" : "Configuración de las notificaciones",
    "Enable" : "Habilitar",
    "Disable" : "Deshabilitar"
},
"nplurals=2; plural=(n != 1);");
