OC.L10N.register(
    "esms",
    {
    "Phone Sync" : "SMS sync",
    "Cancel" : "Avbryt",
    "Confirm" : "Bekreft",
    "Settings" : "Innstillinger",
    "Default country code" : "Standard landskode",
    "Last message" : "Siste melding",
    "Label" : "Etikett",
    "Notification settings" : "Innstillinger for notifiseringer",
    "Enable" : "Aktiver",
    "Disable" : "Deaktiver"
},
"nplurals=2; plural=(n != 1);");
