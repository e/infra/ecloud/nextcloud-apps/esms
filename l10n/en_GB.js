OC.L10N.register(
    "esms",
    {
    "Phone Sync" : "SMS sync",
    "An app to sync SMS with your cloud" : "An app to sync SMS with your cloud",
    "Cancel" : "Cancel",
    "Confirm" : "Confirm",
    "Settings" : "Settings",
    "Label" : "Label",
    "Notification settings" : "Notification settings",
    "Enable" : "Enable",
    "Disable" : "Disable"
},
"nplurals=2; plural=(n != 1);");
