OC.L10N.register(
    "esms",
    {
    "Phone Sync" : "SMS sync",
    "An app to sync SMS with your cloud" : " Aplikaĵo por sinkronigi SMS al via nubo",
    "Cancel" : "Nuligi",
    "Confirm" : "Konfirmi",
    "Settings" : "Agordoj",
    "Enable" : "Ŝalti la opcion",
    "Disable" : "Malŝalti"
},
"nplurals=2; plural=(n != 1);");
