OC.L10N.register(
    "esms",
    {
    "Phone Sync" : "SMS sync",
    "Cancel" : "Cancelar",
    "Confirm" : "Confirmar",
    "Settings" : "Definições",
    "Label" : "Legenda",
    "Notification settings" : "Definições de notificação",
    "Enable" : "Ativar",
    "Disable" : "Desativar"
},
"nplurals=2; plural=(n != 1);");
