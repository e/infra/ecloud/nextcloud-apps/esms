OC.L10N.register(
    "esms",
    {
    "Phone Sync" : "SMS sync",
    "Cancel" : "Peruuta",
    "Confirm" : "Vahvista",
    "No contact found." : "Yhteystietoa ei löydy.",
    "Settings" : "Asetukset",
    "Default country code" : "Oletusmaakoodi",
    "Contact ordering" : "Yhteystietojen järjestäminen",
    "Notification settings" : "Ilmoitusasetukset",
    "Enable" : "Ota käyttöön",
    "Disable" : "Poista käytöstä"
},
"nplurals=2; plural=(n != 1);");
