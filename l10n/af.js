OC.L10N.register(
    "esms",
    {
    "Phone Sync" : "SMS sync",
    "Cancel" : "Kanselleer",
    "Confirm" : "Bevestig",
    "Settings" : "Instellings",
    "Label" : "Etiket",
    "Enable" : "Aktiveer",
    "Disable" : "Deaktiveer"
},
"nplurals=2; plural=(n != 1);");
