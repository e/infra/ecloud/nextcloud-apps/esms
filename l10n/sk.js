OC.L10N.register(
    "esms",
    {
    "Phone Sync" : "SMS sync",
    "Cancel" : "Zrušiť",
    "Confirm" : "Potvrdiť",
    "Settings" : "Nastavenia",
    "Last message" : "Posledná správa",
    "Label" : "Štítok",
    "Notification settings" : "Nastavenie notifikácií",
    "Enable" : "Povoliť",
    "Disable" : "Zakázať"
},
"nplurals=4; plural=(n % 1 == 0 && n == 1 ? 0 : n % 1 == 0 && n >= 2 && n <= 4 ? 1 : n % 1 != 0 ? 2: 3);");
