OC.L10N.register(
    "esms",
    {
    "Phone Sync" : "SMS sync",
    "Cancel" : "Annullér",
    "Confirm" : "Bekræft",
    "Settings" : "Indstillinger",
    "Label" : "Mærkat",
    "Notification settings" : "Meddelelsesindstillinger",
    "Enable" : "Aktivér",
    "Disable" : "Deaktiver"
},
"nplurals=2; plural=(n != 1);");
