OC.L10N.register(
    "esms",
    {
    "Phone Sync" : "SMS sync",
    "Cancel" : "Utzi",
    "Confirm" : "Berretsi",
    "No contact found." : "Ez da kontakturik aurkitu.",
    "Settings" : "Ezarpenak",
    "Last message" : "Azken mezua",
    "Label" : "Etiketa",
    "Reverse ?" : "Alderantzikatu ?",
    "Notification settings" : "Jakinarazpen-ezarpenak",
    "Enable" : "Aktibatu",
    "Disable" : "Desaktibatu",
    "Reset all messages" : "Berrezarri mezu guztiak"
},
"nplurals=2; plural=(n != 1);");
