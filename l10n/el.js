OC.L10N.register(
    "esms",
    {
    "Phone Sync" : "SMS sync",
    "Cancel" : "Ακύρωση",
    "Confirm" : "Επιβεβαίωση",
    "Settings" : "Ρυθμίσεις",
    "Default country code" : "Προεπιλεγμένος κωδικός χώρας",
    "Last message" : "Τελευταίο μήνυμα",
    "Label" : "Ετικέτα",
    "Notification settings" : "Ρυθμίσεις ειδοποιήσεων",
    "Enable" : "Ενεργοποίηση",
    "Disable" : "Απενεργοποίηση"
},
"nplurals=2; plural=(n != 1);");
