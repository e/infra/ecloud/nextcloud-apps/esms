<?php
/**
 * Nextcloud - Phone Sync
 *
 * This file is licensed under the Affero General Public License version 3 or
 * later. See the COPYING file.
 *
 * @author Loic Blot <loic.blot@unix-experience.fr>
 * @contributor: stagprom <https://github.com/stagprom/>
 * @copyright Loic Blot 2014-2017
 */

namespace OCA\ESms\Lib;

use \OCP\Contacts\IManager as IContactsManager;

use \OCA\ESms\Db\ConfigMapper;
use \OCA\DAV\CardDAV\CardDavBackend;
use \OCA\DAV\CardDAV\AddressBook;
class ContactCache {
	/**
	* @var array used to cache the parsed contacts for every request
	*/
	private $contacts;
	private $contactsInverted;
	private $contactPhotos;
	private $contactUids;

	private $cfgMapper;
	private $contactsManager;
	private $cardDavBackend;

	public function __construct (ConfigMapper $cfgMapper, IContactsManager $contactsManager, $cardDavBackend, $db, $userId) {
		$this->contacts = array();
		$this->cardDavBackend = $cardDavBackend;
		$this->db = $db;
		$this->contactPhotos = array();
		$this->contactUids = array();
		$this->contactsInverted = array();
		$this->userId = $userId;
		$this->cfgMapper = $cfgMapper;
		$this->contactsManager = $contactsManager;
	}

	public function getContacts() {
		// Only load contacts if they aren't in the buffer
		if(count($this->contacts) == 0) {
			$this->loadContacts();
		}
		return $this->contacts;
	}

	public function getInvertedContacts() {
		// Only load contacts if they aren't in the buffer
		if(count($this->contactsInverted) == 0) {
			$this->loadContacts();
		}
		return $this->contactsInverted;
	}

	public function getContactPhotos() {
		// Only load contacts if they aren't in the buffer
		if(count($this->contactPhotos) == 0) {
			$this->loadContacts();
		}
		return $this->contactPhotos;
	}

	public function getContactUids() {
		// Only load contacts if they aren't in the buffer
		if(count($this->contactUids) == 0) {
			$this->loadContacts();
		}
		return $this->contactUids;
	}

	/**
	 * Partially importe this function from owncloud Chat app
	 * https://github.com/owncloud/chat/blob/master/app/chat.php
	 */
	private function loadContacts() {
		$this->contacts = array();
		$this->contactsInverted = array();

		// Cache country because of loops
                $configuredCountry = $this->cfgMapper->getCountry();

		$cm = $this->contactsManager;
		if ($cm == null) {
			return;
		}
		$addressBooks = $this->cardDavBackend->getAddressBooksForUser("principals/users/".$this->userId);
		$objects = [];

		foreach($addressBooks as $addressBook) {
			$contactIdName = array();
			$contactIdPhone = array();
			$contactIdPhoto = array();
			$contactIdUID = array();

			$simCardList = array();
			$qb = $this->db->getQueryBuilder();
			$qb
			->select('name', 'value', 'cardid')
			->from('cards_properties')
				->where($qb->expr()->andX(
					$qb->expr()->eq('addressbookid',  $qb->createNamedParameter((int)$addressBook['id'])),
					)
				);
			$result = $qb->execute();

			while ($row = $result->fetch()) {
				if($row['name'] == "FN"){
					$contactIdName[$row['cardid']] = $row['value'];
				}
				if($row['name'] == "TEL"){
					$contactIdPhone[$row['cardid']] = $row['value'];
				}
				if($row['name'] == "PHOTO"){
					$contactIdPhoto[$row['cardid']] = preg_replace("#^VALUE=uri:#","",$row['value'], 1);
				}
				if($row['name'] == "UID"){
					$contactIdUID[$row['cardid']] = $row['value'];
				}

			}
		}
	
		foreach($contactIdPhone as $id => $phoneNumber){
			$name = $contactIdName[$id];
			$this->pushPhoneNumberToCache($phoneNumber, $name, $configuredCountry);
			$this->contactUids[$name] = $contactIdUID[$id];
			if(isset($contactIdPhoto[$id]))
				$this->contactPhotos[$name] = $contactIdPhoto[$id];
		}

	}

	private function pushPhoneNumberToCache($rawPhone, $contactName, $country) {
		$phoneNb = PhoneNumberFormatter::format($country, $rawPhone);
		$this->contacts[$phoneNb] = $contactName;
		// Inverted contacts
		if (!isset($this->contactsInverted[$contactName])) {
			$this->contactsInverted[$contactName] = array();
		}
		array_push($this->contactsInverted[$contactName], $phoneNb);
	}
};

?>
